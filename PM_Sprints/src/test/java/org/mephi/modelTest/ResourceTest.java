package org.mephi.modelTest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mephi.model.Resource;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class ResourceTest
{
    @Test
    @DisplayName("Тест 1: создание ресурса")
    public void CreateRes()
    {
        Resource resource = new Resource("test1", "test1");
    }

    @Test
    @DisplayName("Тест 2: получение данных ресурса")
    public void AllGettersRes()
    {
        String loginGet, roleGet;
        Resource resource = new Resource("test1", "test1");

        loginGet = resource.getLogin();
        roleGet = resource.getRole();
    }
}
