package org.mephi.modelTest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mephi.model.ProjectTask;
import org.mephi.model.Resource;
import org.mephi.model.Sprint;
import org.mephi.model.Task;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class ProjectTaskTest
{
    @Test
    @DisplayName("Тест 1: создание проектного задания")
    public void CreatePT1()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2018,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2018,7,19).getTime();
        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", new LinkedHashSet<Task>(), new LinkedHashSet<Sprint>(), new LinkedHashSet<Resource>(), 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);
    }

    @Test
    @DisplayName("Тест 2: создание проектного задания")
    public void CreatePT2()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2018,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2018,7,19).getTime();
        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", new LinkedHashSet<Resource>(), 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);
    }

    @Test
    @DisplayName("Тест 3: создание проектного задания")
    public void CreatePT3()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2018,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2018,7,19).getTime();
        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", new LinkedHashSet<Task>(),  new LinkedHashSet<Resource>(), 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);
    }

    @Test
    @DisplayName("Тест 4: получение данных из проектного задания")
    public void AllGettersPT()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2011,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2011,7,19).getTime();

        Date startDateGet = new GregorianCalendar(2011,0,14).getTime();
        Date plannedEndDateGet = new GregorianCalendar(2011,5,14).getTime();
        Date facticalEndDateGet = new GregorianCalendar(2011,5,14).getTime();

        String namePT, descr, statusPT;
        Float percentCompletedGet;
        LinkedHashSet<Sprint> sprints = new LinkedHashSet<Sprint>();
        LinkedHashSet<Sprint> sprintsGet = new LinkedHashSet<Sprint>();

        LinkedHashSet<Resource> resources = new LinkedHashSet<Resource>();
        LinkedHashSet<Resource> resourcesGet = new LinkedHashSet<Resource>();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();
        LinkedHashSet<Task> tasksGet = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1","description1", "taskType1", "Open", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2","description2", "taskType2", "Open", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3","description3", "taskType3", "Close", "taskPriority3", 3.3f, res3);

        Sprint sprint1 = new Sprint("sprintName1",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus1");
        Sprint sprint2 = new Sprint("sprintName2",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus2");
        Sprint sprint3 = new Sprint("sprintName3",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus3");

        resources.add(res1);
        resources.add(res2);
        resources.add(res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        sprint1.addTask(task1);
        sprint2.addTask(task2);
        sprint3.addTask(task3);

        sprints.add(sprint1);
        sprints.add(sprint2);
        sprints.add(sprint3);

        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", tasks, sprints, resources, 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);

        //получение данных геттерами

        namePT = projectTaskTest.getProjectTaskName();
        descr = projectTaskTest.getDescription();
        tasksGet = projectTaskTest.getTask();
        sprintsGet = projectTaskTest.getSprint();
        resourcesGet = projectTaskTest.getTeamMembers();
        percentCompletedGet = projectTaskTest.getPercentCompleted();
        startDateGet = projectTaskTest.getStartDate();
        facticalEndDateGet = projectTaskTest.getFacticalEndDate();
        plannedEndDateGet = projectTaskTest.getPlannedEndDate();
        statusPT = projectTaskTest.getProjectStatus();

        /*
        projectTaskTest.addTask(task1);
        projectTaskTest.addTask(task2);
        projectTaskTest.addTask(task3);

        projectTaskTest.addSprint(sprint1);
        projectTaskTest.addSprint(sprint2);
        projectTaskTest.addSprint(sprint3);
        */
    }

    @Test
    @DisplayName("Тест 5: заполнение данных проектного задания")
    public void AllSettersPT()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2018,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2018,7,19).getTime();

        String namePT = "projectTaskName", descr = "description", statusPT= "projectStatus";
        Float percentCompleted = 33f;
        LinkedHashSet<Sprint> sprints = new LinkedHashSet<Sprint>();

        LinkedHashSet<Resource> resources = new LinkedHashSet<Resource>();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1","description1", "taskType1", "Open", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2","description2", "taskType2", "Close", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3","description3", "taskType3", "Open", "taskPriority3", 3.3f, res3);

        Sprint sprint1 = new Sprint("sprintName1",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus1");
        Sprint sprint2 = new Sprint("sprintName2",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus2");
        Sprint sprint3 = new Sprint("sprintName3",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus3");

        resources.add(res1);
        resources.add(res2);
        resources.add(res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        sprint1.addTask(task1);
        sprint2.addTask(task2);
        sprint3.addTask(task3);

        sprints.add(sprint1);
        sprints.add(sprint2);
        sprints.add(sprint3);

        ProjectTask projectTaskTest = new  ProjectTask( "", "", new LinkedHashSet<Task>(), new LinkedHashSet<Sprint>(), new LinkedHashSet<Resource>(), 33f, "", new Date(), new Date(), new Date());

        //заполнение данных сеттерами

        projectTaskTest.setProjectTaskName(namePT);
        projectTaskTest.setDescription(descr);
        projectTaskTest.setTask(tasks);
        projectTaskTest.setSprint(sprints);
        projectTaskTest.setTeamMembers(resources);
        projectTaskTest.setPercentCompleted(percentCompleted);
        projectTaskTest.setStartDate(startDate);
        projectTaskTest.setFacticalEndDate(facticalEndDate);
        projectTaskTest.setPlannedEndDate(plannedEndDate);
        projectTaskTest.setProjectStatus(statusPT);

        /*
        projectTaskTest.addTask(task1);
        projectTaskTest.addTask(task2);
        projectTaskTest.addTask(task3);

        projectTaskTest.addSprint(sprint1);
        projectTaskTest.addSprint(sprint2);
        projectTaskTest.addSprint(sprint3);
        */
    }

    @Test
    @DisplayName("Тест 6: добавить задание в проектную задачу")
    public void AddTaskPT()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2011,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2011,7,19).getTime();

        LinkedHashSet<Sprint> sprints = new LinkedHashSet<Sprint>();

        LinkedHashSet<Resource> resources = new LinkedHashSet<Resource>();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1","description1", "taskType1", "Open", "taskPriority1", 1f, res1);
        Task task2 = new Task("name2","description2", "taskType2", "Closed", "taskPriority2", 1f, res2);
        Task task3 = new Task("name3","description3", "taskType3", "Open", "taskPriority3", 2f, res3);

        Sprint sprint1 = new Sprint("sprintName1",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus1");
        Sprint sprint2 = new Sprint("sprintName2",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus2");
        Sprint sprint3 = new Sprint("sprintName3",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus3");

        resources.add(res1);
        resources.add(res2);
        resources.add(res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        sprint1.addTask(task1);
        sprint2.addTask(task2);
        sprint3.addTask(task3);

        sprints.add(sprint1);
        sprints.add(sprint2);
        sprints.add(sprint3);

        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", new LinkedHashSet<Task>(), sprints, resources, 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);

        //добавить задания в задачу

        projectTaskTest.addTask(task1);
        projectTaskTest.addTask(task2);
        projectTaskTest.addTask(task3);
        /*
        projectTaskTest.addSprint(sprint1);
        projectTaskTest.addSprint(sprint2);
        projectTaskTest.addSprint(sprint3);
        */
    }

    @Test
    @DisplayName("Тест 7: добавить спринт в проектную задачу")
    public void AddSprintPT()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2011,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2011,7,19).getTime();

        LinkedHashSet<Sprint> sprints = new LinkedHashSet<Sprint>();

        LinkedHashSet<Resource> resources = new LinkedHashSet<Resource>();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1","description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2","description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3","description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        Sprint sprint1 = new Sprint("sprintName1",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus1");
        Sprint sprint2 = new Sprint("sprintName2",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus2");
        Sprint sprint3 = new Sprint("sprintName3",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus3");

        resources.add(res1);
        resources.add(res2);
        resources.add(res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        sprint1.addTask(task1);
        sprint2.addTask(task2);
        sprint3.addTask(task3);

        sprints.add(sprint1);
        sprints.add(sprint2);
        sprints.add(sprint3);

        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", tasks, new LinkedHashSet<Sprint>(), resources, 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);

        //добавить задания в задачу
        /*
        projectTaskTest.addTask(task1);
        projectTaskTest.addTask(task2);
        projectTaskTest.addTask(task3);
         */
        projectTaskTest.addSprint(sprint1);
        projectTaskTest.addSprint(sprint2);
        projectTaskTest.addSprint(sprint3);

    }

    @Test
    @DisplayName("Тест 8: удалить задание из проектной задачи")
    public void DelTaskPT()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2011,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2011,7,19).getTime();

        LinkedHashSet<Sprint> sprints = new LinkedHashSet<Sprint>();

        LinkedHashSet<Resource> resources = new LinkedHashSet<Resource>();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1","description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2","description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3","description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        Sprint sprint1 = new Sprint("sprintName1",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus1");
        Sprint sprint2 = new Sprint("sprintName2",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus2");
        Sprint sprint3 = new Sprint("sprintName3",  startDate, new LinkedHashSet<Task>(),  plannedEndDate, "sprintStatus3");

        resources.add(res1);
        resources.add(res2);
        resources.add(res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        sprint1.addTask(task1);
        sprint2.addTask(task2);
        sprint3.addTask(task3);

        sprints.add(sprint1);
        sprints.add(sprint2);
        sprints.add(sprint3);

        ProjectTask projectTaskTest = new ProjectTask( "projectTaskName", "description", tasks, sprints, resources, 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);

        //удалить задание из задачи

        projectTaskTest.deleteTask(task1);

        /*
        projectTaskTest.addSprint(sprint1);
        projectTaskTest.addSprint(sprint2);
        projectTaskTest.addSprint(sprint3);
        */
    }
}
