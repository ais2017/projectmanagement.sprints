package org.mephi.modelTest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mephi.model.ProjectTask;
import org.mephi.model.Resource;
import org.mephi.model.Task;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class TasksTest
{
    @Test
    @DisplayName("Тест 1: создание задания")
    public void CreateTest1()
    {
        Resource res1 = new Resource("login1","role1");
        Task task = new Task("task1", "desc1", res1);
    }

    @Test
    @DisplayName("Тест 2: создание задания")
    public void CreateTest2()
    {
        Resource res1 = new Resource("login1","role1");
        Task task = new Task("task1", "desc1", "taskType", "taskStatus","taskPrior", 33f, res1);
    }

    @Test
    @DisplayName("Тест 3: получение данных через геттеры")
    public void AllGettersTest()
    {
        String nameGet, descGet, typeGet, statusGet, priorityGet;
        Float appraisal;
        Resource res1 = new Resource("login1","role1");
        Resource resGet = new Resource("","");
        Task task = new Task("task1", "desc1", "taskType", "taskStatus","taskPrior", 33f, res1);

        nameGet = task.getName();
        descGet = task.getDescription();
        typeGet = task.getTaskType();
        statusGet = task.getTaskStatus();
        priorityGet = task.getTaskPriority();
        appraisal = task.getTaskAppraisal();
        resGet = task.getExecutor();
    }

    @Test
    @DisplayName("Тест 4: заполнение данных через сеттеры")
    public void AllSettersTest()
    {
        Date startDate = new GregorianCalendar(2018,0,19).getTime();
        Date plannedEndDate = new GregorianCalendar(2018,7,19).getTime();
        Date facticalEndDate = new GregorianCalendar(2018,7,19).getTime();

        String name = "tast1", desc = "desc1", type = "taskType", status = "Close", priority = "taskPrior";
        Float appraisal = 33f;
        Resource res1 = new Resource("login1","role1");
        Task task = new Task("", "", "", "Open","", new Float(0), new Resource("",""));

        ProjectTask projectTaskTest = new  ProjectTask( "projectTaskName", "description", new LinkedHashSet<Task>(),  new LinkedHashSet<Resource>(), 33f, "projectStatus", startDate, facticalEndDate, plannedEndDate);

        task.setName(name);
        task.setDescription(desc);
        task.setTaskType(type);
        task.setTaskPriority(priority);
        task.setTaskAppraisal(appraisal);
        task.setExecutor(res1);

        projectTaskTest.addTask(task);

        task.setTaskStatus(status, projectTaskTest);
    }
}
