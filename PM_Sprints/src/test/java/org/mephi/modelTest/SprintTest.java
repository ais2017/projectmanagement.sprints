package org.mephi.modelTest;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mephi.model.Resource;
import org.mephi.model.Sprint;
import org.mephi.model.Task;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashSet;

@TestInstance(TestInstance.Lifecycle.PER_METHOD)
public class SprintTest {
    @Test
    @DisplayName("Тест 1: создание спринта")
    public void CreateSprint1() {
        Sprint sprint = new Sprint("name");
    }

    @Test
    @DisplayName("Тест 2: создание спринта")
    public void CreateSprint2() {
        Date startDate = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDate = new GregorianCalendar(2018, 7, 19).getTime();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1", "description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2", "description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3", "description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        Sprint sprint = new Sprint("name", startDate, tasks, endDate, "status");
    }

    @Test
    @DisplayName("Тест 3: получение значений спринта")
    public void AllGettersSprint() {
        Date startDate = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDate = new GregorianCalendar(2018, 7, 19).getTime();

        Date startDateGet = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDateGet = new GregorianCalendar(2018, 7, 19).getTime();

        String statusGet, nameGet;

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();
        LinkedHashSet<Task> tasksGet = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1", "description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2", "description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3", "description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        Sprint sprint = new Sprint("name", startDate, tasks, endDate, "status");

        nameGet = sprint.getSprintName();
        statusGet = sprint.getSprintStatus();
        tasksGet = sprint.getTask();
        startDateGet = sprint.getSprintStartDate();
        endDateGet = sprint.getSprintEndDate();
    }

    @Test
    @DisplayName("Тест 4: выставление значений спринта")
    public void AllSettersSprint() {
        Date startDate = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDate = new GregorianCalendar(2018, 7, 19).getTime();

        String status = "statusSprint", name = "sprintName";

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1", "description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2", "description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3", "description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        Sprint sprint = new Sprint("", new Date(), new LinkedHashSet<Task>(), new Date(), "");

        sprint.setSprintName(name);
        sprint.setSprintStatus(status);
        sprint.setTask(tasks);
        sprint.setSprintStartDate(startDate);
        sprint.setSprintEndDate(endDate);
    }

    @Test
    @DisplayName("Тест 5: добавление задания в спринт")
    public void AddTaskSprint() {
        Date startDate = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDate = new GregorianCalendar(2018, 7, 19).getTime();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1", "description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2", "description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3", "description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        Sprint sprint = new Sprint("name", startDate, new LinkedHashSet<Task>(), endDate, "status");

        sprint.addTask(task1);
        sprint.addTask(task2);
        sprint.addTask(task3);
    }

    @Test
    @DisplayName("Тест 6: удаление задания из  спринта")
    public void DelTaskSprint() {
        Date startDate = new GregorianCalendar(2018, 0, 19).getTime();
        Date endDate = new GregorianCalendar(2018, 7, 19).getTime();

        LinkedHashSet<Task> tasks = new LinkedHashSet<Task>();

        Resource res1 = new Resource("test1", "test2");
        Resource res2 = new Resource("test3", "test4");
        Resource res3 = new Resource("test5", "test6");

        Task task1 = new Task("name1", "description1", "taskType1", "taskStatus1", "taskPriority1", 3.1f, res1);
        Task task2 = new Task("name2", "description2", "taskType2", "taskStatus2", "taskPriority2", 3.2f, res2);
        Task task3 = new Task("name3", "description3", "taskType3", "taskStatus3", "taskPriority3", 3.3f, res3);

        tasks.add(task1);
        tasks.add(task2);
        tasks.add(task3);

        Sprint sprint = new Sprint("name", startDate, tasks, endDate, "status");

        sprint.deleteTask(task1);
    }
}