package org.mephi.model;

public class Task
{
    private String name;
    private String description;
    private String taskType;
    private String taskStatus;
    private String taskPriority;
    private Float taskAppraisal;
    private Resource executor;

    public Task(String name, String description, Resource executor)
    {
        this.name = name;
        this.description = description;
        this.executor = executor;
    }

    public Task(String name, String description, String taskType, String taskStatus, String taskPriority, Float taskAppraisal, Resource executor)
    {
        this.name = name;
        this.description = description;
        this.taskType = taskType;
        this.taskStatus = taskStatus;
        this.taskPriority = taskPriority;
        this.taskAppraisal = taskAppraisal;
        this.executor = executor;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public String getTaskType()
    {
        return taskType;
    }

    public void setTaskType(String taskType)
    {
        this.taskType = taskType;
    }

    public String getTaskStatus() {
        return taskStatus;
    }

    public void setTaskStatus(String taskStatus, ProjectTask projectTask)
    {
        if (this.taskAppraisal.equals(0) || this.taskAppraisal.toString().isEmpty())
        {
            throw new RuntimeException("Cannot set status because of empty task appraisal.");
        }
        this.taskStatus = taskStatus;

        if (taskStatus.equals("Close"))
        {
            projectTask.setNewPercent(this.taskAppraisal);
        }
    }

    public String getTaskPriority() {
        return taskPriority;
    }

    public void setTaskPriority(String taskPriority)
    {
        this.taskPriority = taskPriority;
    }

    public Float getTaskAppraisal()
    {
        return taskAppraisal;
    }

    public void setTaskAppraisal(Float taskAppraisal)
    {
        this.taskAppraisal = taskAppraisal;
    }

    public Resource getExecutor()
    {
        return executor;
    }

    public void setExecutor(Resource executor)
    {
        this.executor = executor;
    }
}
