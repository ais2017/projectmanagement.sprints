package org.mephi.model;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;

public class ProjectTask {
    private String projectTaskName;
    private String description;
    private LinkedHashSet<Task> tasks;
    private LinkedHashSet<Sprint> sprints;
    private LinkedHashSet<Resource> teamMembers;
    private Float percentCompleted;
    private String projectStatus;
    private Date startDate;
    private Date facticalEndDate;
    private Date plannedEndDate;

    public ProjectTask(String projectTaskName, String description, LinkedHashSet<Task> tasks, LinkedHashSet<Sprint> sprints, LinkedHashSet<Resource> teamMembers, Float percentCompleted, String projectStatus, Date startDate, Date facticalEndDate, Date plannedEndDate) {
        this.projectTaskName = projectTaskName;
        this.description = description;
        this.tasks = tasks;
        this.sprints = sprints;
        this.teamMembers = teamMembers;
        this.setNewPercent(percentCompleted);
        this.projectStatus = projectStatus;
        this.startDate = startDate;
        this.facticalEndDate = facticalEndDate;
        this.plannedEndDate = plannedEndDate;
    }

    public ProjectTask(String projectTaskName, String description, LinkedHashSet<Resource> teamMembers, Float percentCompleted, String projectStatus, Date startDate, Date facticalEndDate, Date plannedEndDate) {
        this.projectTaskName = projectTaskName;
        this.description = description;
        this.teamMembers = teamMembers;
        this.percentCompleted = percentCompleted;
        this.projectStatus = projectStatus;
        this.startDate = startDate;
        this.facticalEndDate = facticalEndDate;
        this.plannedEndDate = plannedEndDate;
    }

    public ProjectTask(String projectTaskName, String description, LinkedHashSet<Task> tasks, LinkedHashSet<Resource> teamMembers, Float percentCompleted, String projectStatus, Date startDate, Date facticalEndDate, Date plannedEndDate) {
        this.projectTaskName = projectTaskName;
        this.description = description;
        this.tasks = tasks;
        this.teamMembers = teamMembers;
        this.setNewPercent(percentCompleted);
        this.projectStatus = projectStatus;
        this.startDate = startDate;
        this.facticalEndDate = facticalEndDate;
        this.plannedEndDate = plannedEndDate;
    }

    public String getProjectTaskName() {
        return projectTaskName;
    }

    public void setProjectTaskName(String projectTaskName) {
        this.projectTaskName = projectTaskName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LinkedHashSet<Task> getTask() {
        return tasks;
    }

    public void setTask(LinkedHashSet<Task> tasks) {
        this.tasks = tasks;
        this.setNewPercent(0f);
    }

    public LinkedHashSet<Sprint> getSprint() {
        return sprints;
    }

    public void setSprint(LinkedHashSet<Sprint> sprints) {
        this.sprints = sprints;
    }

    public LinkedHashSet<Resource> getTeamMembers() {
        return teamMembers;
    }

    public void setTeamMembers(LinkedHashSet<Resource> teamMembers) {
        this.teamMembers = teamMembers;
    }

    public Float getPercentCompleted() {
        return percentCompleted;
    }

    public void setPercentCompleted(Float percentCompleted)
    {
        this.setNewPercent(percentCompleted);
    }

    public void setNewPercent(Float taskAppraisal)
    {
       Float percentPerTask = new Float (0), PercentTask = new Float(0), allTaskAppr = new Float (0);

       LinkedHashSet<Task> projTasks = this.getTask();

       if (projTasks.isEmpty())
       {
           PercentTask = 0f;
       }
       else
       {
           PercentTask = 0f;

           List<Task> arrPT = new ArrayList<Task>(projTasks);

           for (int i = 0; i < arrPT.size(); i++)
               allTaskAppr += arrPT.get(i).getTaskAppraisal();

           percentPerTask = 100 / allTaskAppr;

           for (int j = 0; j < arrPT.size(); j++) {
               if (arrPT.get(j).getTaskStatus().equals("Closed")) {
                   PercentTask += arrPT.get(j).getTaskAppraisal() * percentPerTask;
               }
           }
       }
       this.percentCompleted = PercentTask;
    }

    public String getProjectStatus()
    {
        return projectStatus;
    }

    public void setProjectStatus(String projectStatus)
    {
        this.projectStatus = projectStatus;
    }

    public Date getStartDate()
    {
        return startDate;
    }

    public void setStartDate(Date startDate)
    {
        this.startDate = startDate;
    }

    public Date getFacticalEndDate()
    {
        return facticalEndDate;
    }

    public void setFacticalEndDate(Date facticalEndDate)
    {
        if (this.startDate.toString().isEmpty())
        {
            throw new RuntimeException("Cannot set factical end date with empty start date");
        }
        if (facticalEndDate.before(this.startDate))
        {
            throw new RuntimeException("Cannot set facical end dateless then start date");
        }
        this.facticalEndDate = facticalEndDate;
    }

    public Date getPlannedEndDate()
    {
        return plannedEndDate;
    }

    public void setPlannedEndDate(Date plannedEndDate)
    {
        if (this.startDate.toString().isEmpty())
        {
            throw new RuntimeException("Cannot set planned end date with empty start date");
        }
        if (plannedEndDate.before(this.startDate))
        {
            throw new RuntimeException("Cannot set planned end date less then start date");
        }
        this.plannedEndDate = plannedEndDate;
    }

    public void addTask(Task task)
    {
        if (task == null)
            throw new RuntimeException("Cannot add null task");
        this.tasks.add(task);
        this.setNewPercent(0f);
    }

    public void addSprint(Sprint sprint)
    {
        if (sprint == null)
            throw new RuntimeException("Cannot add null sprint");
        this.sprints.add(sprint);
    }

    public void deleteTask(Task task)
    {
        this.tasks.remove(task);
        this.setNewPercent(0f);
    }
}
