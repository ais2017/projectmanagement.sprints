package org.mephi.model;

import java.util.Date;
import java.util.LinkedHashSet;

public class Sprint
{
    private String sprintName;
    private Date sprintStartDate;
    private LinkedHashSet<Task> tasks;
    private Date sprintEndDate;
    private String sprintStatus;

    public Sprint(String sprintName)
    {
        this.sprintName = sprintName;
    }
    public Sprint(String sprintName, Date sprintStartDate, LinkedHashSet<Task> tasks, Date sprintEndDate, String sprintStatus)
    {
        this.sprintName = sprintName;
        this.sprintStartDate = sprintStartDate;
        this.tasks = tasks;
        this.sprintEndDate = sprintEndDate;
        this.sprintStatus = sprintStatus;
    }

    public String getSprintName()
    {
        return sprintName;
    }

    public void setSprintName(String sprintName)
    {
        this.sprintName = sprintName;
    }

    public Date getSprintStartDate()
    {
        return sprintStartDate;
    }

    public void setSprintStartDate(Date sprintStartDate)
    {
        this.sprintStartDate = sprintStartDate;
    }

    public LinkedHashSet<Task> getTask()
    {
        return tasks;
    }

    public void setTask(LinkedHashSet<Task> task)
    {
        this.tasks = task;
    }

    public void addTask(Task task) {
        if (task == null)
            throw new RuntimeException("Cannot add null task");
        this.tasks.add(task);
    }

    public void deleteTask(Task task) {
        this.tasks.remove(task);
    }

    public Date getSprintEndDate()
    {
        return sprintEndDate;
    }

    public void setSprintEndDate(Date sprintEndDate)
    {
        if (this.sprintStartDate.toString().isEmpty())
        {
            throw new RuntimeException("Cannot set planned end date with empty start date");
        }
        if (sprintEndDate.before(this.sprintStartDate))
        {
            throw new RuntimeException("Cannot set planned end date less then start date");
        }
        this.sprintEndDate = sprintEndDate;
    }

    public String getSprintStatus()
    {
        return sprintStatus;
    }

    public void setSprintStatus(String sprintStatus)
    {
        this.sprintStatus = sprintStatus;
    }
}
