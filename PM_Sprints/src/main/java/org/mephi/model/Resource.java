package org.mephi.model;

public class Resource
{
    private String login;
    private String role;

    public Resource(String login, String role)
    {
        this.login = login;
        this.role = role;
    }

    public String getLogin()
    {
        return login;
    }

    public String getRole()
    {
        return role;
    }
}
